package iam45794365.escoladeltreball.org.sqliteold;

import android.app.Activity;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {
    //nom de la base de dades
    private static final String nom_DB = "musica.db";
    //query per crear la taula
    private static final String Create_Table = "create table if not exists africa (id integer primary key,titol text unique,autor text,pais text,any integer); ";
    //query per introduir les cançons
    private static final String insert_songs = "insert into africa(titol,autor,pais,any) values('My people','Youssou N Dour','Senegal','1994'),('Vuka Vuka','The Manhattan Brothers','South Africa','1954'),('Mother of Hope','Ladysmith Black Mambazo','South Africa','1973'),('Desert roots','Hamid Baroudi','Argelia','1994');";
    //query per mostrar contingut
    private static final String mostrar_Contingut = "select * from africa;";
    private Button btn;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(this);
        tv = (TextView) findViewById(R.id.tv);

    }


    @Override
    public void onClick(View v) {
        //creamos la base de datos
        SQLiteDatabase db = openOrCreateDatabase(nom_DB, MODE_PRIVATE, null);
        afegirContingut(db);
        mostrarContingut(db);
    }

    public void afegirContingut(SQLiteDatabase db) {
        db.execSQL(Create_Table);
        try {
            db.execSQL(insert_songs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void mostrarContingut(SQLiteDatabase db) {
        Cursor cur = db.rawQuery(mostrar_Contingut, null);
        cur.moveToFirst();
        for (int i = 0; i < cur.getCount(); i++) {
            //recorrem i afegim al text view
            tv.setText(tv.getText() + "" + cur.getInt(0) + "|" + cur.getString(1) + "|" + cur.getString(2) + "|" + cur.getString(3) + "|" + cur.getInt(4) + "\n");
            cur.moveToNext();
        }
    }
}
