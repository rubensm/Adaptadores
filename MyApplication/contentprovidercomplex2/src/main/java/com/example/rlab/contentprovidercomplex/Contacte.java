package com.example.rlab.contentprovidercomplex;

/**
 * Created by rlab on 29/02/2016.
 */
public class Contacte {
    String nom;
    String id;


    public Contacte(String nom, String id) {
        this.nom = nom;
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Contacte{" +
                "nom='" + nom + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
