package com.example.rlab.contentprovidercomplex;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {
    private Button genAll;
    private EditText contingut;
    private Button showBad;
    private Button showGood;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        genAll = (Button) findViewById(R.id.btnLoadAll);
        genAll.setOnClickListener(this);
        showBad = (Button) findViewById(R.id.btntv);
        showBad.setOnClickListener(this);
        showGood = (Button) findViewById(R.id.btnlv);
        showGood.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        contingut = (EditText) findViewById(R.id.et);
        Intent i = new Intent(this, InfoContacts.class);
        i.putExtra("carregaTot", v.getId());
        if (v.getId() == R.id.btnlv || v.getId() == R.id.btntv) {
            i.putExtra("TextName", contingut.getText().toString());
        }
        startActivity(i);
    }


}
