package com.example.rlab.contentprovidercomplex;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class InfoContacts extends Activity implements AdapterView.OnItemClickListener {
    private TextView all;
    ArrayList<Contacte> llistaContactes = new ArrayList<Contacte>();
    int btnid;
    String name;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        btnid = getIntent().getExtras().getInt("carregaTot");
        if (btnid == R.id.btnlv) {
            name = getIntent().getExtras().getString("TextName");
            setContentView(R.layout.list_view);
            lv = (ListView) findViewById(R.id.lv);

            lv.setAdapter(new AdaptadorContacte(this, carregaConcreta(name)));
            lv.setOnItemClickListener(this);
        } else if (btnid == R.id.btntv) {
            name = getIntent().getExtras().getString("TextName");
            setContentView(R.layout.activity_main2);
            carregaConcretUser(name);
        } else if (btnid == R.id.btnLoadAll) {
            setContentView(R.layout.activity_main2);
            carrega();
        }
    }

    public void carrega() {
        int contador = 0;
        all = (TextView) findViewById(R.id.tv);
        Uri content_uri = ContactsContract.Contacts.CONTENT_URI;
        StringBuffer output = new StringBuffer();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(content_uri, null, null, null, null);
        //loop for every contact in the phone
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                output.append("contacte: " + contador + "\n");
                contador++;
                for (int i = 0; i < cur.getColumnCount(); i++) {
                    output.append(cur.getColumnName(i) + ": ");
                    output.append(cur.getString(i));
                    output.append("\n");
                }
                output.append("\n");

            }
            all.setText(output);
        }
    }

    public void carregaConcretUser(String name) {
        int contador = 0;
        all = (TextView) findViewById(R.id.tv);
        Uri content_uri = ContactsContract.Contacts.CONTENT_URI;
        StringBuffer output = new StringBuffer();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(content_uri, null, null, null, null);
        //loop for every contact in the phone
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                if (cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)).contains(name)) {
                    output.append("contacte: " + contador + "\n");
                    contador++;
                    for (int i = 0; i < cur.getColumnCount(); i++) {
                        output.append(cur.getColumnName(i) + ": ");
                        output.append(cur.getString(i));
                        output.append("\n");
                    }
                    output.append("\n");
                }
            }
            all.setText(output);
        }
    }

    public void concretUser(String name) {
        int contador = 0;
        all = (TextView) findViewById(R.id.tv);
        Uri content_uri = ContactsContract.Contacts.CONTENT_URI;
        StringBuffer output = new StringBuffer();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(content_uri, null, null, null, null);
        //loop for every contact in the phone
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                if (cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)).equals(name)) {
                    output.append("contacte: " + contador + "\n");
                    contador++;
                    for (int i = 0; i < cur.getColumnCount(); i++) {
                        output.append(cur.getColumnName(i) + ": ");
                        output.append(cur.getString(i));
                        output.append("\n");
                    }
                    output.append("\n");
                }
            }
            all.setText(output);
        }
    }

    public ArrayList<Contacte> carregaConcreta(String text) {
        Cursor cur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (cur.moveToNext()) {
            String name = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String id = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            if (name.contains(text)) {
                Contacte c = new Contacte(name, id);
                llistaContactes.add(c);
            }
        }
        cur.close();
        return llistaContactes;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Contacte c = (Contacte) ((ListView) findViewById(R.id.lv)).getItemAtPosition(position);
        setContentView(R.layout.activity_main2);
        concretUser(c.getNom());
    }
}

