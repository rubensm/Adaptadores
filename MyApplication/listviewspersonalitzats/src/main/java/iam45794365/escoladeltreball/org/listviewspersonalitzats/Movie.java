package iam45794365.escoladeltreball.org.listviewspersonalitzats;

/**
 * Created by iam45794365 on 1/11/16.
 */
public class Movie {
    private String titol;
    private int any;
    private int posicio;

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public int getAny() {
        return any;
    }

    public void setAny(int any) {
        this.any = any;
    }

    public int getPosicio() {
        return posicio;
    }

    public void setPosicio(int posicio) {
        this.posicio = posicio;
    }
}
