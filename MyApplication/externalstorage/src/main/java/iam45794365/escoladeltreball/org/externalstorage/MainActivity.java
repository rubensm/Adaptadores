package iam45794365.escoladeltreball.org.externalstorage;

import android.app.Activity;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity implements View.OnClickListener {
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    EditText name;
    EditText content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
        btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(this);
        btn4 = (Button) findViewById(R.id.btn4);
        btn4.setOnClickListener(this);
        name = (EditText) findViewById(R.id.edt1);
        content = (EditText) findViewById(R.id.edt2);

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn1) {
            readFromStaticFile();
        }
        if (v.getId() == R.id.btn2) {
            try {
                readFromGivenName(name.getText().toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (v.getId() == R.id.btn3) {
            writeInInternalStorage(name.getText().toString(), content);
        }
        if (v.getId() == R.id.btn4) {
            try {
                writeInExternalStorage(name.getText().toString(),content);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void readFromStaticFile() {
        InputStream in = getResources().openRawResource(R.raw.file);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line;
        String contingut = "";
        try {
            while ((line = br.readLine()) != null) {
                contingut += line + "\n";
            }
            Toast.makeText(MainActivity.this, contingut, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFromGivenName(String name) throws FileNotFoundException {
        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), name);
        if (file.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                String content = "";
                while ((line = br.readLine()) != null) {
                    content += line + "\n";
                }
                Toast.makeText(MainActivity.this, content, Toast.LENGTH_SHORT).show();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(MainActivity.this, "El fitxer no existeix", Toast.LENGTH_SHORT).show();
        }
    }

    public void writeInInternalStorage(String fileName, EditText et) {
        try {
            FileOutputStream out = openFileOutput(fileName, MODE_PRIVATE);
            OutputStreamWriter ow = new OutputStreamWriter(out);
            ow.write(et.getText().toString() + " ");
            ow.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeInExternalStorage(String fileName, EditText et) throws IOException {
        // ruta fins la sdcard
        File sdcard = Environment.getExternalStorageDirectory();
        // creem el fitxer i afegim el seu contingut
        File file = new File(sdcard, fileName);
        FileOutputStream os = new FileOutputStream(file);
        String data = et.getText().toString();
        os.write(data.getBytes());
        os.close();
    }

}
