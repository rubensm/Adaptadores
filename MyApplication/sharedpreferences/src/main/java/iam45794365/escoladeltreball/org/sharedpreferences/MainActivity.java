package iam45794365.escoladeltreball.org.sharedpreferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    private EditText user;
    private EditText passwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user = (EditText) findViewById(R.id.login);
        passwd = (EditText) findViewById(R.id.password);
        Button b = (Button) findViewById(R.id.btn);
        b.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        Toast.makeText(this, "Per raons de seguretat no es pot mostra la password de " + user.getText().toString(), Toast.LENGTH_SHORT).show();
        SharedPreferences sp1 = getSharedPreferences("dades", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp1.edit();
        editor.putString("user", user.getText().toString());
        editor.putString("password", passwd.getText().toString());
        editor.commit();
    }
}
