package iam45794365.escoladeltreball.org.tractamentxml;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;

import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class RssParserPull {


    private URL rssUrl; // Emmagatzemem la URL

	/*
     * Constructor de la classe rep com a argument l'enllaç url
	 */

    public RssParserPull(String url) {
        try {
            this.rssUrl = new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /*
     * Mètode que extreu les diferents notícies que conté una certa url (feed, rss)
	 * retorna una col·lecció (List) de news a partir de la URL que se li passa
	*/
    public List<Noticia> parse() {
        List<Noticia> news = null;
        XmlPullParser parser = Xml.newPullParser();

        try {
            parser.setInput(this.getInputStream(), null);
            int event = parser.getEventType();
            Noticia currentNew = null;
            // Mentre no arribem al tag final
            while (event != XmlPullParser.END_DOCUMENT) {
                String tag = null;
                switch (event) {
                    // Si ens trobem el tag de començament de document
                    // construim un array de notícies
                    case XmlPullParser.START_DOCUMENT:
                        news = new ArrayList<Noticia>();
                        break;
                    // Si ens trobem un tag de començament diferent de l'anterior,
                    // mirem de quin tipus es i ho asignem on toqui
                    //mirem si es inici de tag
                    case XmlPullParser.START_TAG:
                        tag = parser.getName();
                        // si el tag de començament és item creem un objecte noticia
                        if (tag.equals("item")) {
                            currentNew = new Noticia();
                        } else if (currentNew != null) { // si anteriorment ja haviem trobat un tag item discriminem
                            // Si el tag és un link parsegem el text
                            // i l'afegim al camp adient de l'objecte notícia actual
                            if (tag.equals("link")) {
                                currentNew.setLink(parser.nextText());
                            }
                            //si es descripcio, s'afegeix al camp adient
                            else if (tag.equals("description")) {
                                currentNew.setDescription(parser.nextText());
                            }
                            //si es la data de publicacio,es posa al camp andient de l'objecte noticia
                            else if (tag.equals("pubDate")) {
                                currentNew.setDate(parser.nextText());
                            }
                            //si es el titol, es posa on toqui de l'objecte
                            else if (tag.equals("title")) {
                                currentNew.setTitle(parser.nextText());
                            }
                            //
                            else if (tag.equals("guid")) {
                                currentNew.setGuid(parser.nextText());
                            }
                        }
                        break;
                    // Si el tag és de tancament, sabem que de final de document no és,
                    // mirem de quin tipus és i discriminem
                    case XmlPullParser.END_TAG:
                        tag = parser.getName();
                        // Si el tag és de final de item, ja hem acabat de recollir la informació i afegim la notícia a la col·lecció
                        //if (tag.equals("item") && currentNew != null) {
                        if (tag.equals("item")) {
                            news.add(currentNew);
                        }
                        break;
                }
                //avancem el parser
                event = parser.next();
            }

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return news;
    }

    //per conectarse a internet
    private InputStream getInputStream() {
        try {
            return rssUrl.openConnection().getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}