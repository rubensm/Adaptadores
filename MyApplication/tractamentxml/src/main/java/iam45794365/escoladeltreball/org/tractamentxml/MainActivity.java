package iam45794365.escoladeltreball.org.tractamentxml;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button loadButton;
    private TextView resultTxt;
    private List<Noticia> news;
    private String rssLink = "http://www.elmundotoday.com/feed/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflem el layout
        setContentView(R.layout.activity_main);
        // Recuperem els objectes inflats
        loadButton = (Button) findViewById(R.id.btnLoad);
        resultTxt = (TextView) findViewById(R.id.txtResult);
        // Registrem el botó de carrega
        loadButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // Amb tasca asíncrona
        LoadXmlTask task = new LoadXmlTask();
        task.execute(rssLink);
    }

    // Tasca asincrona per carregar el fitxer XML en segon pla (background)
    private class LoadXmlTask extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {
            RssParserPull rssParser = new RssParserPull(params[0]);
            news = rssParser.parse();
            return true;
        }

        protected void onPostExecute(Boolean result) {

            // Tractem la llista de notícies
            // Per exemple: escrivim els títols en pantalla
            resultTxt.setText("");
            int newsNumber = news.size();
            for (int i = 0; i < newsNumber; i++) {
                resultTxt.setText(resultTxt.getText().toString() + System.getProperty("line.separator")
                        + news.get(i).getTitle());
            }
        }

    }


}