package iam45794365.escoladeltreball.org.llistaopcions;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //agafem el contingut i el guardem a un array
        String[] coses = getResources().getStringArray(R.array.diferentThings);
        //creem adaptador i l'assignem
        //Primer parametre contexte,segon parametre layout,tercer parametre array de dades
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, coses);
        //assignem l'adptador
        setListAdapter(adaptador);

    }

    @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
        // l The ListView where the click happened
        // v The view that was clicked within the ListView
        // position The position of the view in the list
        // id The row id of the item that was clicked

        switch (position) {
            case 0:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com")));
                break;
            case 1:
                startActivity(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI));
                break;
            case 2:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("tel:")));
                break;
            case 3:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:")));
                break;
        }
    }


}
