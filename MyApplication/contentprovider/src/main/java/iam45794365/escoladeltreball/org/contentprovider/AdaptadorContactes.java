package iam45794365.escoladeltreball.org.contentprovider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by iam45794365 on 2/24/16.
 */

public class AdaptadorContactes extends BaseAdapter {
    ArrayList<Contacte> con = new ArrayList<Contacte>();
    LayoutInflater li;

    public AdaptadorContactes(Context context, ArrayList<Contacte> con) {
        this.con = con;
        this.li = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return con.size();
    }

    @Override
    public Object getItem(int position) {
        return con.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView nom;
        TextView telf;
        TextView id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.items, null);
            holder = new ViewHolder();
            holder.nom = (TextView) convertView.findViewById(R.id.nom);
            holder.telf = (TextView) convertView.findViewById(R.id.telf);
            holder.id = (TextView) convertView.findViewById(R.id.id);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.nom.setText("" + con.get(position).getNom());
        holder.telf.setText("" + con.get(position).getTelf());
        holder.id.setText("" + con.get(position).getId());
        return convertView;
    }


}