package iam45794365.escoladeltreball.org.contentprovider;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity implements AdapterView.OnItemClickListener {
    ListView lv;
    ArrayList<Contacte> llistaContactes = new ArrayList<Contacte>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //recogemos el listview
        lv = (ListView) findViewById(R.id.lv);
        //asignamos el adaptador customizado con el arraylist de contactos
        lv.setAdapter(new AdaptadorContactes(this, carregaAgenda()));
        //añadimos la funcion para cuando sea clickado
        lv.setOnItemClickListener(this);

    }

    /*
    public void carrega() {
        Uri content_uri = ContactsContract.Contacts.CONTENT_URI;
        StringBuffer output = new StringBuffer();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(content_uri, null, null, null, null);
        //loop for every contact in the phone
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                for (int i = 0; i < cur.getColumnCount(); i++) {
                    output.append(cur.getColumnName(i) + ": ");
                    output.append(cur.getString(i));
                    output.append("\n");
                }
                output.append("\n");
            }
            //aqui aniria l'asignació-->textview.setText(output);
        }

    }
    */

    public ArrayList<Contacte> carregaAgenda() {
        Cursor cur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (cur.moveToNext()) {
            String name = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String id = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            Contacte c = new Contacte(name, phoneNumber, id);
            llistaContactes.add(c);
        }
        cur.close();
        return llistaContactes;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Contacte c = (Contacte) ((ListView) findViewById(R.id.lv)).getItemAtPosition(position);
        Intent i = new Intent(MainActivity.this, Main2Activity.class);
        i.putExtra("Contacte", c);
        startActivity(i);
    }
}