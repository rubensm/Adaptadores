package iam45794365.escoladeltreball.org.contentprovider;

import java.io.Serializable;

/**
 * Created by iam45794365 on 2/24/16.
 */

public class Contacte implements Serializable {
    private String nom;
    private String telf;
    private String id;

    public String getTelf() {
        return telf;
    }

    public void setTelf(String telf) {
        this.telf = telf;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Contacte(String nom, String telf, String id) {
        this.nom = nom;
        this.telf = telf;
        this.id = id;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Contacte{" +
                "nom='" + nom + '\'' +
                ", telf='" + telf + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}