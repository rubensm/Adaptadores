package iam45794365.escoladeltreball.org.listviews;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //agafem contingut de values en un array
        String[] pelicules = getResources().getStringArray(R.array.movie_title);
        //creem adaptador
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, R.layout.row, R.id.textoAmostrar, pelicules);
        ListView lv =(ListView)findViewById(R.id.listview);
        lv.setAdapter(adaptador);
        lv.setTextFilterEnabled(true);



    }

   /* @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
        // l The ListView where the click happened
        // v The view that was clicked within the ListView
        // position The position of the view in the list
        // id The row id of the item that was clicked
        //fem un cast de la view per poder agafar el text
        TextView tv = (TextView) view;
        Toast.makeText(this, "Parent de la vista: " + l + " \nVista: " + view +
                " \nPosition: " + position + " \nId: " + id + " \nText: " + tv.getText().toString(), Toast.LENGTH_SHORT).show();
    }
    */

    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
