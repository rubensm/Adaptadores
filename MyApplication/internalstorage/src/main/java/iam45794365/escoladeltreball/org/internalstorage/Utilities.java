package iam45794365.escoladeltreball.org.internalstorage;

import android.content.Context;
import android.widget.EditText;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;


public class Utilities {


    public static void write(EditText et, String fileName, Context context) {
        try {
            FileOutputStream out = context.openFileOutput(fileName, context.MODE_PRIVATE);
            OutputStreamWriter ow = new OutputStreamWriter(out);
            ow.write(et.getText().toString() + " ");
            et.setText("");
            ow.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
