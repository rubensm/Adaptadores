package iam45794365.escoladeltreball.org.internalstorage;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity implements View.OnClickListener {
    final static String fileName = "files";
    static final int READ_BLOCK_SIZE = 100;
    private Button write;
    private Button append;
    private Button read;
    private Button staticFile;
    EditText et;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et = (EditText) findViewById(R.id.et);
        write = (Button) findViewById(R.id.write);
        write.setOnClickListener(this);
        append = (Button) findViewById(R.id.append);
        append.setOnClickListener(this);
        read = (Button) findViewById(R.id.read);
        read.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.write) {
            Utilities.write(et, fileName, this);
        }
        if (v.getId() == R.id.append) {
            try {
                FileOutputStream out = openFileOutput(fileName, MODE_APPEND);
                OutputStreamWriter ow = new OutputStreamWriter(out);
                ow.write(et.getText().toString() + " ");
                et.setText("");
                ow.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (v.getId() == R.id.read) {
            try {
                FileInputStream fileIn = openFileInput(fileName);
                InputStreamReader ir = new InputStreamReader(fileIn);
                char[] inputBuffer = new char[READ_BLOCK_SIZE];
                String s = "";
                int charRead;
                while ((charRead = ir.read(inputBuffer)) > 0) {
                    // char to string conversion
                    String readString = String.copyValueOf(inputBuffer, 0, charRead);
                    s += readString;
                }
                ir.close();
                Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
